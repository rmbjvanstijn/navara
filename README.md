## Explanation application

This application uses the floodfill algorithm to count the different groups in a matrix.
There is a image added just like the test to show which example will be tested.

## Running the application
- Open the folder of the app in the terminal where you can find the .sln
- Run 'dotnet publish -c Release -r win10-x64'
- Open the folder bin/release/netcoreapp2.0/win10-x64/ and run NavaraExercise3.exe
