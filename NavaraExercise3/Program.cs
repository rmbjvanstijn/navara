﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NavaraExercise3
{
    class Program
    {
        //Example for this exercise, see Example.JPG for visual matrix
        private static readonly int[][] matrix =
        {
            new [] { 1, 2, 3},
            new [] { 2, 2, 3},
            new [] { 4, 4, 3 },
            new [] { 4, 3, 3 },
            new [] { 5, 3, 3 },
            new [] { 5, 5, 5 },
        };

        private static readonly int rows = matrix.Length;
        
        //Assume that all rows has same amount of columns
        private static readonly int columns = matrix.FirstOrDefault().Length;
        public static List<int[,]> usableMatrixFields = new List<int[,]>();
        public static int count = 0;

        static void Main(string[] args)
        {
            Console.WriteLine("Matrix to count the groups:" + Environment.NewLine);
            for (var i = 0; i < rows; i++)
            {
                for (var j= 0; j < matrix[i].Length; j++)
                {
                    Console.Write(matrix[i][j] + " ");
                }
                Console.WriteLine();
            }

            Console.WriteLine(Environment.NewLine);

            var columnsTotal = rows * columns;

            //Creates a list for usable matrixfields
            for (var i = 0; i < rows; i++)
            {
                for (var j = 0; j < matrix[i].Length; j++)
                {
                    usableMatrixFields.Add(new int[i,j]);
                }
            }

            var k = 0;

            //loops over every matrix column, checks if it's already checked and calculates new group by matrixvalue
            while (k < columnsTotal)
            {
                var matrixValue = usableMatrixFields.ElementAt(k);

                if (matrixValue == null)
                {
                    continue;
                }

                var x = matrixValue.GetLength(0);
                var y = matrixValue.GetLength(1);

                if (matrix[x][y] == 10000000)
                {
                    k++;
                    continue;
                }

                FloodFillAlgorithm(x, y, matrix[x][y]);
                count++;
                k++;
            }
           
            Console.WriteLine($"Total fields: {count}");
            Console.ReadLine();
        }

        private static void FloodFillAlgorithm(int x, int y, int matrixGroupValue)
        {
            while (true)
            {
                // return when x and y are out of scope
                if (x < 0 || x >= rows || y < 0 || y >= columns)
                {
                    return;
                }

                //if matrixValue doesn't match the groupvalue return that specific FloodFillAlgorithm
                if (matrix[x][y] != matrixGroupValue)
                {
                    return;
                }

                //Sets new value so all the matrix values are only used once
                matrix[x][y] = 10000000;

                //Checks al possible surroundings
                FloodFillAlgorithm(x + 1, y, matrixGroupValue);
                FloodFillAlgorithm(x - 1, y, matrixGroupValue);
                FloodFillAlgorithm(x, y + 1, matrixGroupValue);
                y = y - 1;
            }
        }
    }
}
